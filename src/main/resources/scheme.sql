drop table if exists students, groups, courses, students_courses;

create table groups(
    group_id SERIAL PRIMARY KEY,
    group_name varchar NOT NULL
);

create table courses(
    course_id SERIAL PRIMARY KEY,
    course_name varchar NOT NULL,
    course_description varchar NOT NULL
);

create table students(
    student_id SERIAL PRIMARY KEY,
    firstname varchar NOT NULL,
    lastname varchar NOT NULL,
    group_name varchar,
    group_id int,
    course_id int,
    FOREIGN KEY (group_id) REFERENCES groups(group_id),
    FOREIGN KEY (course_id) REFERENCES courses(course_id)
);

create table students_courses(
    student_id int NOT NULL,
    course_id int NOT NULL,
    PRIMARY KEY(student_id, course_id)
);