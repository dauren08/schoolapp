package schoolapp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.sql.DataSource;

import model.Course;
import model.Group;
import model.Student;

public class DataJoiner {
    
    private DataSource dataSource;
    private ResourceReader resourceReader;
    private final int numberOfStudents = 200;
    private final int numberOfGroups = 10;
    private static final int ALPHABET_LENGTH = 26;
    private Random random = new Random();

    public DataJoiner(DataSource dataSource, ResourceReader resourceReader) {
        this.dataSource = dataSource;
        this.resourceReader = resourceReader;
    }

    public List<Student> createStudents(String firstNamesFile, String lastNamesFile) throws IOException {
        List<Student> studentsArray = new ArrayList<Student>();

        String[] firstNameArray = resourceReader.readFile(firstNamesFile).split(System.lineSeparator());
        String[] lastNameArray = resourceReader.readFile(lastNamesFile).split(System.lineSeparator());

        for (int i = 0; i < numberOfStudents; i++) {
            studentsArray.add(new Student(firstNameArray[random.nextInt(firstNameArray.length)],
                    lastNameArray[random.nextInt(lastNameArray.length)]));
        }

        return studentsArray;
    }

    public List<Course> createCourses(String string) throws FileNotFoundException, IOException {
        List<Course> courseList = new ArrayList<>();
        String[] str = resourceReader.readFile(string).split(System.lineSeparator());
        for (String line : str) {
            String[] course = line.split(",");
            courseList.add(new Course(course[0], course[1]));
        }

        return courseList;
    }

    public List<Group> createGroups() {
        List<Group> groupList = new ArrayList<>();
        String[] str = generateRandomGroupNames().split(System.lineSeparator());
        for (String line : str) {
            groupList.add(new Group(line));
        }

        return groupList;
    }

    private String generateRandomGroupNames() {
        StringBuilder stringBuilder = new StringBuilder();
        String generatedNames;

        for (int i = 0; i < numberOfGroups; i++) {

            char firstLetter = (char) (random.nextInt(ALPHABET_LENGTH) + 'A');
            char secondLetter = (char) (random.nextInt(ALPHABET_LENGTH) + 'A');
            char firstDigit = (char) (random.nextInt(10) + '0');
            char secondDigit = (char) (random.nextInt(10) + '0');

            stringBuilder.append(firstLetter).append(secondLetter).append("-").append(firstDigit).append(secondDigit)
                    .append(System.lineSeparator());

        }
        generatedNames = stringBuilder.toString();

        return generatedNames;
    }
}
