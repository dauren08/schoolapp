package schoolapp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class ResourceReader {

    public String readFile(String fileName) throws IOException {
        StringBuilder sb = new StringBuilder();
        String lines;
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
        try (BufferedReader file = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = file.readLine()) != null) {
                sb.append(line + System.lineSeparator());
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        }

        lines = sb.toString();

        return lines;
    }      
}
