package schoolapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

import org.apache.ibatis.jdbc.ScriptRunner;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import dao.CourseDaoImpl;
import dao.GroupDaoImpl;
import dao.StudentDaoImpl;
import model.Course;
import model.Group;
import model.Student;

public class SchoolApplication {

    public static void main(String[] args) throws IOException, URISyntaxException { 
       
        String courseFile = "courses.txt";
        String firstNameFile = "firstNames.txt";
        String lastNameFile = "lastNames.txt";
        String schemeSql = "scheme.sql";
        ResourceReader resourceReader = new ResourceReader();

        DataSource dataSource = new HikariDataSource(new HikariConfig("/database.properties"));

        try (Connection connection = dataSource.getConnection()) {
            ScriptRunner sr = new ScriptRunner(connection);
            InputStream is = SchoolApplication.class.getResourceAsStream("/" + schemeSql);
            Reader reader = new BufferedReader(new InputStreamReader(is));
            sr.runScript(reader);

        } catch (SQLException e) {
            throw new IllegalStateException();
        }

        StudentDaoImpl studentDAO = new StudentDaoImpl(dataSource);
        GroupDaoImpl groupDAO = new GroupDaoImpl(dataSource);
        CourseDaoImpl courseDAO = new CourseDaoImpl(dataSource);
        DataJoiner dataJoiner = new DataJoiner(dataSource, resourceReader);

        List<Group> groups = dataJoiner.createGroups();
        groupDAO.create(groups);

        List<Student> students = dataJoiner.createStudents(firstNameFile, lastNameFile);
        studentDAO.create(students);

        List<Course> courses = dataJoiner.createCourses(courseFile);
        courseDAO.create(courses);

        List<Student> allStudents = studentDAO.getAll();
        List<Group> allGroups = groupDAO.getAll();
        List<Course> allCourses = courseDAO.getAll();

        studentDAO.updateStudentsWithAssignmentToGroupsAndCourses(allStudents, allGroups, allCourses);

        List<Pair<Integer, Integer>> pairs = studentDAO.createIdPairs();
        for (Pair<Integer, Integer> pair : pairs) {
            studentDAO.createJunctionTableWithStudentsAndCourses(pair);
        }

        List<Student> st = studentDAO.getStudentsWithCourses();
        for (Student sts : st) {
            //System.out.println(sts + System.lineSeparator());

        }
//        String actual = resourceReader.readFile("testCourses.txt");
//        System.out.println(actual);
          
        
      

    }
}
