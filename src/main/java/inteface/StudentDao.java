package inteface;

import java.util.List;
import java.util.Optional;

import model.Student;

public interface StudentDao {
    void create(Student student);

    void create(List<Student> studentList);

    void update(int id, Student updatedStudent);

    void delete(int id);

    Optional<Student> getById(int id);

    List<Student> getAll();
}
