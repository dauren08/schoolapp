package inteface;

import java.util.List;
import java.util.Optional;

import model.Course;

public interface CourseDao {
    List<Course> getAll();

    void create(Course course);

    Optional<Course> getById(int id);

    void update(Course updatedCourse);

    void delete(int id);

    void create(List<Course> courseList);
}
