package inteface;

import java.util.List;
import java.util.Optional;

import model.Group;

public interface GroupDao {
    List<Group> getAll();

    void create(Group group);

    Optional<Group> getById(int id);

    void update(int id, Group group);

    void delete(int id);

    void create(List<Group> groupList);

}
