package model;

import java.util.List;
import java.util.Objects;

public class Student {
    
    private Integer id;
    private String firstName;
    private String lastName;
    private Group group;
    private List<Course> courses;

    public Student(String firstName, String lastName) {
        this(1, firstName, lastName, new Group(null));
    }

    public Student(int id, String firstName, String lastName, Group group) {
        this(id, firstName, lastName, group, null);
    }

    public Student(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(Integer id, String firstName, String lastName, Group group, List<Course> courses) {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.group = group;
        this.courses = courses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @Override
    public int hashCode() {
        return Objects.hash(courses, firstName, group, id, lastName);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        return Objects.equals(courses, other.courses) && Objects.equals(firstName, other.firstName)
                && Objects.equals(group, other.group) && Objects.equals(id, other.id)
                && Objects.equals(lastName, other.lastName);
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", group=" + group
                + ", courses=" + courses + "]";
    }

    
}
