package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.sql.DataSource;

import inteface.StudentDao;
import model.Course;
import model.Group;
import model.Student;
import schoolapp.Pair;

public class StudentDaoImpl implements StudentDao {
    private final DataSource dataSource;
    private Random random = new Random();
    CourseDaoImpl courseDaoImpl;

    public StudentDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void createJunctionTableWithStudentsAndCourses(Pair<Integer, Integer> pair) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO students_courses(student_id, course_id) VALUES (?, ?)");

            ps.setInt(1, pair.getT1());
            ps.setInt(2, pair.getT2());

            ps.addBatch();
            ps.executeBatch();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public List<Pair<Integer, Integer>> createIdPairs() {
        List<Pair<Integer, Integer>> pairs = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT students.student_id, courses.course_id FROM students INNER JOIN courses ON students.course_id=courses.course_id");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                pairs.add(new Pair<Integer, Integer>(rs.getInt("student_id"), rs.getInt("course_id")));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return pairs;
    }

    public List<Student> getStudentsWithCourses() {
        List<Student> students = new ArrayList<>();
        List<Course> courses = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT students.student_id, students.firstname, students.lastname, students.group_id, students.course_id, students.group_name, courses.course_name, courses.course_description FROM students "
                            + "JOIN students_courses ON students.student_id = students_courses.student_id "
                            + "JOIN courses ON students_courses.course_id = courses.course_id "
                            + "ORDER BY students.student_id", ResultSet.TYPE_SCROLL_SENSITIVE, 
                            ResultSet.CONCUR_UPDATABLE);
            ps.addBatch();
            ps.executeBatch();

            ResultSet rs = ps.executeQuery();
            
            Integer currentStudentId = null;
            Integer previousStudentId = null;
           
            while (rs.next()) {                
                currentStudentId = rs.getInt("student_id");
                          
                if(currentStudentId == previousStudentId || previousStudentId == null) {
                    currentStudentId = rs.getInt("student_id");
                    courses.add(new Course(rs.getInt("course_id"), rs.getString("course_name"), rs.getString("course_description")));
                    previousStudentId = currentStudentId;
                    rs.next();
                
                } else {
                    rs.previous();
                    courses.add(new Course(rs.getInt("course_id"), rs.getString("course_name"), rs.getString("course_description")));   
                    rs.next();
                }   
                
                students.add(new Student(rs.getInt("student_id"), rs.getString("firstname"), rs.getString("lastname"),
                        new Group(rs.getInt("group_id"), rs.getString("group_name")), courses));
                
                courses = new ArrayList<>();
                 
                previousStudentId = currentStudentId;
        
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return students;
    }

    @Override
    public List<Student> getAll() {
        List<Student> students = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT student_id, firstname, lastname, group_id, group_name FROM students");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getInt("student_id"), rs.getString("firstname"),
                        rs.getString("lastname"), new Group(rs.getInt("group_id"), rs.getString("group_name")));
                students.add(student);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return students;
    }

    @Override
    public void create(Student student) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO students(firstname, lastname) VALUES (?, ?)");

            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());

            ps.addBatch();
            ps.executeBatch();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void create(List<Student> studentList) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO students(firstname, lastname) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);

            for (Student student : studentList) {
                ps.setString(1, student.getFirstName());
                ps.setString(2, student.getLastName());

                ps.addBatch();
            }
            ps.executeBatch();
            ResultSet rs = ps.getGeneratedKeys();
            
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<Student> getById(int id) {
        Optional<Student> student = null;
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT student_id, firstname, lastname, group_id  FROM students WHERE student_id=?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            rs.next();

            student = Optional.of(new Student(rs.getInt("student_id"), rs.getString("firstname"),
                    rs.getString("lastname"), new Group(rs.getInt("group_id"), rs.getString("group_name"))));

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return student;
    }

    public void updateStudentsWithAssignmentToGroupsAndCourses(List<Student> allStudents, List<Group> allGroups,
            List<Course> allCourses) {

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE students SET firstname=?, lastname=?, group_name=?, group_id=?, course_id=? WHERE student_id=?");
            for (Student st : allStudents) {
                Group group = allGroups.get(random.nextInt(allGroups.size()));
                Course course = allCourses.get(random.nextInt(allCourses.size()));
                ps.setString(1, st.getFirstName());
                ps.setString(2, st.getLastName());
                ps.setString(3, group.getName());
                ps.setInt(4, group.getId());
                ps.setInt(5, course.getId());
                ps.setInt(6, st.getId());

                ps.addBatch();

            }

            ps.executeBatch();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(int id, Student updatedStudent) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection
                    .prepareStatement("UPDATE students SET firstname=?, lastname=?, group_id=? WHERE student_id=?");

            ps.setString(1, updatedStudent.getFirstName());
            ps.setString(2, updatedStudent.getLastName());
            ps.setInt(3, updatedStudent.getGroup().getId());
            ps.setInt(4, id);

            ps.executeQuery();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(int id) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement("DELETE FROM students WHERE student_id=?");

            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
