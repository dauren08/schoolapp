package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import inteface.CourseDao;
import model.Course;

public class CourseDaoImpl implements CourseDao {

    private final DataSource dataSource;

    public CourseDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Course> getAll() {
        List<Course> courses = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection
                    .prepareStatement("SELECT course_id, course_name, course_description FROM courses");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Course course = new Course(rs.getInt("course_id"), rs.getString("course_name"),
                        rs.getString("course_description"));
                courses.add(course);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return courses;
    }

    @Override
    public void create(Course course) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO courses(course_name, course_description) VALUES (?, ?)");

            ps.setString(1, course.getName());
            ps.setString(2, course.getDescription());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void create(List<Course> courseList) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO courses(course_name, course_description) VALUES (?, ?)",
                    Statement.RETURN_GENERATED_KEYS);

            for (Course course : courseList) {
                ps.setString(1, course.getName());
                ps.setString(2, course.getDescription());

                ps.addBatch();

            }
            ps.executeBatch();
            ResultSet rs = ps.getGeneratedKeys();
            
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<Course> getById(int id) {
        Optional<Course> course;

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT course_id, course_name, course_description FROM courses WHERE course_id=?");

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            rs.next();

            course = Optional.ofNullable(new Course(rs.getInt("course_id"), rs.getString("course_name"),
                    rs.getString("course_description")));

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return course;
    }

    @Override
    public void update(Course updatedCourse) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection
                    .prepareStatement("UPDATE courses SET course_name=?, course_description=? WHERE course_id=?");

            ps.setString(1, updatedCourse.getName());
            ps.setString(2, updatedCourse.getDescription());
            ps.setInt(3, updatedCourse.getId());

            ps.addBatch();
            ps.executeBatch();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM courses WHERE course_id=?");

            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
