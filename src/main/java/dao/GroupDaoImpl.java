package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import inteface.GroupDao;
import model.Group;

public class GroupDaoImpl implements GroupDao {

    private final DataSource dataSource;

    public GroupDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Group> getAll() {
        ArrayList<Group> groups = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement("SELECT group_id, group_name FROM groups");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                groups.add(new Group(rs.getInt("group_id"), rs.getString("group_name")));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return groups;
    }

    @Override
    public void create(Group group) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement("INSERT INTO groups(group_name) VALUES (?)",
                    Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, group.getName());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            group.setId(rs.getInt("group_id"));

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void create(List<Group> groupList) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement("INSERT INTO groups(group_name) VALUES (?)",
                    Statement.RETURN_GENERATED_KEYS);

            for (Group group : groupList) {
                ps.setString(1, group.getName());

                ps.addBatch();

            }
            ps.executeBatch();
            ResultSet rs = ps.getGeneratedKeys();
            
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<Group> getById(int id) {
        Optional<Group> group;

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement("SELECT group_name FROM groups WHERE group_id=?");

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            rs.next();

            group = Optional.of(new Group(rs.getString("group_name")));

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return group;
    }

    @Override
    public void update(int id, Group group) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement("UPDATE groups SET group_name=? WHERE group_id=?");

            ps.setString(1, group.getName());
            ps.setInt(2, id);

            ps.addBatch();
            ps.executeBatch();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(int id) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement ps = connection.prepareStatement("DELETE FROM groups WHERE group_id=?");

            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
