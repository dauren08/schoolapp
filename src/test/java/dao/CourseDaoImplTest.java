package dao;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.utility.DockerImageName;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import model.Course;

class CourseDaoImplTest {

    @Container
    private static PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:latest");;
    
    private DataSource ds = new HikariDataSource(new HikariConfig("/database.properties"));;
    private CourseDaoImpl courseDao = new CourseDaoImpl(ds);
    private DbTestUtils dbTestUtils = new DbTestUtils();
    private final static String DOCKER_IMAGE = "postgres";

    @BeforeAll
    public static void setupContainer() throws SQLException {
        try (PostgreSQLContainer<?> container = new PostgreSQLContainer<>(DockerImageName.parse(DOCKER_IMAGE))
                .withDatabaseName("schoolconsoleapp")
                .withUsername("postgres")
                .withPassword("user")) {
            container.start();           
        }
           
    }

    @BeforeEach
    public void setup() {
        String url = container.getJdbcUrl();
        
        try {
            Connection connection = DriverManager.getConnection(url);
            connection.createStatement().executeQuery("INSERT INTO courses(course_name, course_description) VALUES ('German', 'Learn german language')");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    private static DataSource createDataSource(JdbcDatabaseContainer container) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(container.getJdbcUrl());
        hikariConfig.setUsername(container.getUsername());
        hikariConfig.setPassword(container.getPassword());
        
        return new HikariDataSource(hikariConfig);
    }
    
    private static ResultSet performQuery(DataSource ds, String query) throws SQLException {
        Statement st = ds.getConnection().createStatement();
        st.execute(query);
        
        ResultSet rs = st.getResultSet();
        rs.next();
        return rs;    
    }

    @Test
    public void shouldBeReturnedTrue_whenContainerIsRunning() {
        String url = container.getJdbcUrl(); 
        assertTrue(container.isRunning());
        assertEquals("", url);
    }

    @Test
    public void getById_shouldReturnCourseById() throws SQLException {
        
        long courseId = dbTestUtils.create(ds, "INSERT INTO courses(course_name, course_description) VALUES ('German', 'Learn german language')");
        Object courseExpected = new Course(1, "German", "Learn german language");
        Object courseActual = dbTestUtils.select(ds, "SELECT course_id, course_name, course_description FROM courses WHERE course_id=?", courseId);
        
        assertEquals(courseExpected, courseActual);
    }

    @Test
    public void create_shouldCreateCourse() throws SQLException {
        long courseId = dbTestUtils.create(ds, "INSERT INTO courses(course_name, course_description) VALUES ('German', 'Learn german language')");
        
        Course courseExpected = new Course("German", "Learn german language");
        courseDao.create(courseExpected);
        
        Object courseActual = dbTestUtils.select(ds, "SELECT course_id, course_name, course_description FROM courses WHERE course_id=?", courseId);
        
        assertEquals(courseExpected, courseActual);
        
    }
    
    @Test
    public void update_shouldUpdateCourseById_whenStudentExist() throws SQLException {
        Course courseExpected = new Course(1, "German", "Learn german language");
        
        long courseId = dbTestUtils.create(ds, "INSERT INTO courses(course_name, course_description) VALUES ('German', 'Learn german language')");
        
        courseExpected.setDescription("Speak german language");
        courseDao.update(courseExpected);
        
        Object courseActual = dbTestUtils.select(ds, "SELECT course_id, course_name, course_description FROM courses WHERE course_id=?", courseId);
        
        assertNotNull(courseActual);
        assertEquals(courseExpected, courseActual);       
    }
    
    @Test
    public void delete_shouldDeleteCourseById_whenThisStudentExist() throws SQLException {
        long courseId = dbTestUtils.create(ds, "INSERT INTO courses(course_name, course_description) VALUES ('German', 'Learn german language')");
        courseDao.delete(1);
        
        Object courseActual = dbTestUtils.select(ds, "SELECT course_id, course_name, course_description FROM courses WHERE course_id=?", courseId);
        
        assertNull(courseActual);
          
    }
}
