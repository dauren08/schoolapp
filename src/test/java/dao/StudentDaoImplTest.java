package dao;

import static org.junit.jupiter.api.Assertions.*;
import java.sql.*;
import javax.sql.DataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import model.Course;
import model.Student;

class StudentDaoImplTest {
    private DataSource ds = new HikariDataSource(new HikariConfig("/database.properties"));
    private StudentDaoImpl studentDaoImpl = new StudentDaoImpl(ds);
    private DbTestUtils dbTestUtils = new DbTestUtils();
    private static PostgreSQLContainer<?> postgresContainer = new PostgreSQLContainer<>("postgres:latest");

    @BeforeAll
    public static void setupContainer() throws SQLException {
        postgresContainer.start();

        String url = postgresContainer.getJdbcUrl();
        String username = postgresContainer.getUsername();
        String password = postgresContainer.getPassword();
        Connection connection = DriverManager.getConnection(url, username, password);

        PreparedStatement ps = connection.prepareStatement("CREATE TABLE students(student_id SERIAL PRIMARY KEY, firstname varchar NOT NULL," +
                " lastname varchar NOT NULL," +
                " group_name varchar," +
                " group_id int," +
                " course_id int)");

        ps.executeUpdate();
        //System.out.println(url);
    }
    
    @Test
    public void getById_shouldReturnStudentById() throws SQLException {
        long studentId = dbTestUtils.create(ds, "INSERT INTO students(firstname, lastname) VALUES ('Myke', 'Tyson')");
        Object studExpected = new Student(1, "Myke", "Tyson");
        Object studentActual = dbTestUtils.select(ds, "SELECT student_id, firstname, lastname FROM students WHERE student_id=?", studentId);
        
        assertEquals(studExpected, studentActual);
    }
    
    
    @Test
    public void create_shouldCreateStudent() throws SQLException {
        String url = postgresContainer.getJdbcUrl();
        String username = postgresContainer.getUsername();
        String password = postgresContainer.getPassword();
        Connection connection = DriverManager.getConnection(url, username, password);

        Student studentExpected = new Student("Myke", "Tyson");
        studentDaoImpl.create(studentExpected);

        PreparedStatement ps = connection.prepareStatement("SELECT firstname, lastname, student_id from students");
        ResultSet rs = ps.executeQuery();

        assertEquals(studentExpected.getFirstName(), rs.getString("firstname"));
    }
    
    @Test
    public void update_shouldUpdateStudentById_whenStudentExist() throws SQLException {
        String url = postgresContainer.getJdbcUrl();
        String username = postgresContainer.getUsername();
        String password = postgresContainer.getPassword();
        Connection connection = DriverManager.getConnection(url, username, password);
        Student studentExpected = new Student("Myke", "Tyson");
        studentDaoImpl.create(studentExpected);
        PreparedStatement psCreated = connection.prepareStatement("SELECT firstname, lastname, student_id from students WHERE firstname='Myke'");
        ResultSet rsCreated = psCreated.executeQuery();

        studentExpected.setFirstName("John");
        studentDaoImpl.update(rsCreated.getInt("student_id"), studentExpected);

        PreparedStatement psUpdated = connection.prepareStatement("SELECT firstname, lastname, student_id from students");
        ResultSet rsUpdated = psUpdated.executeQuery();
        
        studentExpected.setFirstName("John");
        studentDaoImpl.update(rsCreated.getInt("student_id"), studentExpected);


        assertEquals(studentExpected.getFirstName(), rsCreated.getString("firstname"));
        assertNotNull(studentExpected);
        assertEquals(studentExpected.getFirstName(), rsUpdated.getString("firstname"));
    }
    
    @Test
    public void delete_shouldDeleteStudentById_whenThisStudentExist() throws SQLException {
        long studentId = dbTestUtils.create(ds, "INSERT INTO students(firstname, lastname) VALUES ('Myke', 'Tyson')");
        studentDaoImpl.delete((int)studentId );

        Object courseActual = dbTestUtils.select(ds, "SELECT course_id, course_name, course_description FROM courses WHERE course_id=?", studentId);
        assertNull(courseActual);
    }
}
