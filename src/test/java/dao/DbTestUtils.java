package dao;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.sql.DataSource;

import model.Student;
import org.junit.jupiter.api.Test;

import model.Course;


class DbTestUtils {
    private DataSource dataSource;
    private final int numberOfStudents = 200;
    private final int numberOfGroups = 10;
    private static final int ALPHABET_LENGTH = 26;
    private Random random = new Random();

    protected DbTestUtils() {}
    
    
    public void createTestTable(DataSource ds, String sql) throws SQLException {
        try (Connection connection = ds.getConnection();
             Statement statement = connection.createStatement()) {
            
            statement.executeUpdate(sql);
        }
    }
    
    public void dropTestTable(DataSource ds, String sql) throws SQLException {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            
            statement.executeUpdate(sql);
        }
    }
    
    public String generateRandomGroupNames() {
        StringBuilder stringBuilder = new StringBuilder();
        String generatedNames;

        for (int i = 0; i < numberOfGroups; i++) {

            char firstLetter = (char) (random.nextInt(ALPHABET_LENGTH) + 'A');
            char secondLetter = (char) (random.nextInt(ALPHABET_LENGTH) + 'A');
            char firstDigit = (char) (random.nextInt(10) + '0');
            char secondDigit = (char) (random.nextInt(10) + '0');

            stringBuilder.append(firstLetter).append(secondLetter).append("-").append(firstDigit).append(secondDigit)
                    .append(System.lineSeparator());

        }
        generatedNames = stringBuilder.toString();

        return generatedNames;
    }

    public long create(DataSource ds, String query) throws SQLException {
      try (Connection connection = ds.getConnection();
           Statement st = connection.createStatement()) {
        st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
        ResultSet keys = st.getGeneratedKeys();
        assert keys.next();
        return keys.getLong(1);
      }
    }
    
    
    public Object select(DataSource ds, String query, Object...params) throws SQLException {
        Student student = null;
        Course course = null;
        
        try(Connection connection = ds.getConnection();
            PreparedStatement ps = connection.prepareStatement(query)) {
            
            ps.setObject(1, params);
            ResultSet rs =ps.executeQuery(); 
            
            course = new Course(rs.getInt("course_id"), rs.getString("course_name"), rs.getString("course_description"));
            student = new Student(rs.getInt("student_id"), rs.getString("firstname"), rs.getString("lastname"));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        
          return student;
        
      }

    public List<Map<String, Object>> selectStudentsWithCourses(DataSource ds, String query, Object... params) throws SQLException {
      try (Connection connection = ds.getConnection();
           PreparedStatement ps = connection.prepareStatement(query)) {
        for (int i = 0; i < params.length; ++i) {
          ps.setObject(i + 1, params[i]);
        }
        ResultSet rs = ps.executeQuery();
        ResultSetMetaData meta = rs.getMetaData();
        List<Map<String, Object>> result = new LinkedList<>();
        while (rs.next()) {
          Map<String, Object> row = new HashMap<>();
          result.add(row);
          for (int i = 1; i <= meta.getColumnCount(); ++i) {
            row.put(meta.getColumnName(i), rs.getObject(i));
          }
        }
        return result;
      }
    }
    
    public List<Map<String, Object>> select(DataSource ds, String query) throws SQLException {
        try (Connection connection = ds.getConnection();
             PreparedStatement ps = connection.prepareStatement(query)) {
          
          ResultSet rs = ps.executeQuery();
          ResultSetMetaData meta = rs.getMetaData();
          List<Map<String, Object>> result = new LinkedList<>();
          while (rs.next()) {
            Map<String, Object> row = new HashMap<>();
            result.add(row);
            for (int i = 1; i <= meta.getColumnCount(); ++i) {
              row.put(meta.getColumnName(i), rs.getObject(i));
            }
          }
          return result;
        }
      }
}
