package schoolapp;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class ResourceReaderTest {
    private ResourceReader resourceReader = new ResourceReader();
    @Test
    void readFile_shouldReturnEveryLineSeparatedByNewLineFromFile() throws IOException {
        String expected = "Math, Solving of exircises" + System.lineSeparator()
                        + "Physic, Solving of exircises" + System.lineSeparator()
                        + "Biology, Solving of exircises" + System.lineSeparator()
                        + "Chemistry, Solving of exircises" + System.lineSeparator()
                        + "Geometry, Solving of exircises" + System.lineSeparator()
                        + "Philosophie, Solving of exircises" + System.lineSeparator()
                        + "English, Solving of exircises" + System.lineSeparator()
                        + "German, Solving of exircises" + System.lineSeparator()
                        + "Informatik, Solving of exircises" + System.lineSeparator()
                        + "Geography, Solving of exircises" + System.lineSeparator();
        String actual = resourceReader.readFile("testCourses.txt");
        
        assertEquals(expected, actual);
    }
}
